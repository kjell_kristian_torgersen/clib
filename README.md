# README #

This is my private collection of much used C functions. From time to time I need to do certain operations like dealing with serial ports, sockets and so on. Instead of rewriting this functionality (which includes studying man pages to remember how syscalls works and so on) from time to time, I've now collected all of it into this library. 
 
Then it is possible to either include all these files in other projects, or compile this to either a static or shared library.
I usually make and put the shared library libclib.so together with the rest of system libraries, so I easily can use these functions from anywhere with minimal effort. 

### What is this repository for? ###

* Serial port as file descriptor io.c
* TCP and UDP, both server and client with sockets io.c
* Very basic file descriptor based timers timer.c
* Pipes as file descriptors io.c
* Posix threads thread.c
* Linked lists implementation list.c
* Byte array array.c
* "Element" array earray.c
* Iterators iterator.c
* String operations (splitting, merging ...) string.c
* Basic logging of information, warnings and errors. clib.c
* File IO helper functions (read/write all lines, read/write all bytes) file.c

### How do I get set up? ###

Go to the src/ folder and type follows to compile and make a shared library (remove or rename to exclude the main.c file):

* gcc -O2 -shared -fpic -o libclib.so *.c

Optionally install on system with the following commands (as superuser)

* cp libclib.so /usr/lib/
* cp clib.h /usr/include/

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact