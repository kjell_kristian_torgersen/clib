#include "clib_priv.h"

static int Range_HasNext(void * state);
static void * Range_Next(void * state);
static void * Range_Get(void * state);
static void Range_Destroy(void * state);

static struct Iteratorvtable dummyvtable = { Range_HasNext, Range_Next, Range_Get, Range_Destroy };

int It_HasNext(Iterator * it)
{
	return it->vtable->HasNext(it->state);
}
void It_Next(Iterator * it)
{
	it->state = it->vtable->Next(it->state);
}

void* It_Get(Iterator * it)
{
	return it->vtable->Get(it->state);
}

void It_Dispose(Iterator * it)
{
	it->vtable->Destroy(it->state);
	free(it);
}

Iterator * Range(int start, int stop)
{
	Iterator * ret = malloc(sizeof(Iterator));
	ret->vtable = &dummyvtable;
	ret->state = malloc(2 * sizeof(int));
	int * i = ret->state;
	i[0] = start;
	i[1] = stop;

	return ret;
}

static int Range_HasNext(void * state)
{
	int * i = (int*) state;
	if (i[0] < i[1])
		return 1;
	else {
		return 0;
	}
}

static void * Range_Get(void * state)
{
	return state;
}

static void * Range_Next(void * state)
{
	int * i = (int*) state;
	(*i)++;
	return state;
}

static void Range_Destroy(void * state)
{
	free(state);
}
