#include <stdlib.h>
#include <stdint.h>
#include <time.h>

typedef struct rng {
	unsigned int MT[624];
	unsigned int index;
} rng_t;

rng_t * RNG_Init(int seed)
{
	rng_t * this = malloc(sizeof(struct rng));
	if (this) {
		unsigned int i;
		this->index = 0;

		if (seed == 0) {
			seed = (int) time(NULL);
		}
		if (seed == 0) {
			seed = 1;
		}

		this->MT[0] = (unsigned int) seed;

		for (i = 1; i < 624; i++) {
			this->MT[i] = (1812433253 * (this->MT[i - 1] ^ (this->MT[i - 1] >> 30)) + i);
		}
	}
	return this;
}

void generate_numbers(rng_t * this)
{
	unsigned int i, y;
	for (i = 0; i < 624; i++) {
		y = (0x8000000 & this->MT[i]) + (0x7fffffff & this->MT[(i + 1) % 624]);
		this->MT[i] = this->MT[(i + 397) % 624] ^ (y >> 1);
		if ((y % 2) == 1) {
			this->MT[i] = this->MT[i] ^ 0x9908b0df;
		}
	}
}

uint32_t RNG_uint32(rng_t * this)
{
	if (this->index == 0) generate_numbers(this);
	unsigned int y = this->MT[this->index];

	y ^= (y >> 11);
	y ^= ((y << 7) & 0x9d2c5680);
	y ^= ((y << 15) & 0xefc60000);
	y ^= (y >> 18);

	this->index = (this->index + 1) % 624;

	return y; // return the random number
}

uint16_t RNG_uint16(rng_t * this)
{
	return RNG_uint32(this) & 0xffff; // return the 16 least significant bits
}

uint8_t RNG_uint8(rng_t * this)
{
	return RNG_uint32(this) & 0xff; // return the 8 least significant bits
}

double RNG_Double(rng_t * this)
{ // make a number between -1.0 and 1.0
	return (double) RNG_uint32(this) / ((double) 0x7fffffff) - 1.0;
}

void RNG_Write(rng_t * this, void * buf, int n)
{
	unsigned char * b = buf;
	for (int i = 0; i < n; i++) {
		b[i] = (unsigned char) RNG_uint32(this);
	}
}

