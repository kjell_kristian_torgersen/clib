#include "clib_priv.h"
#include <pthread.h>

pthread_t Thread_Start(void *(*start_routine) (void *), void * argument)
{
	pthread_t t;
	int status = pthread_create(&t, NULL, start_routine, argument);
	if(status != 0) {
		Warning("Error creating thread.");
	}
	return t;
}

void * Thread_Join(pthread_t th)
{
	void * ret;
	int status = pthread_join(th, &ret);
	if(status != 0) perror("Thread join error: ");
	return ret;
}
