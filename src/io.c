#include "clib_priv.h"

#include <arpa/inet.h>
#include <errno.h>
#include <execinfo.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stddef.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <termios.h>
#include <unistd.h>
#include <stdio.h>
#include <signal.h>

static void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*) sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*) sa)->sin6_addr);
}

int Serialport_Connect(char * device, tcflag_t baud, struct termios * oldtio)
{
	struct termios newtio;

	int fd = open(device, O_RDWR | O_NOCTTY);
	if (fd < 0) {
		perror(device);
		return -1;
	}

	if (oldtio)
		tcgetattr(fd, oldtio); /* save current port settings */

	memset(&newtio, 0, sizeof(newtio));
	newtio.c_cflag = baud | CS8 | CLOCAL | CREAD;
	newtio.c_iflag = IGNPAR;
	newtio.c_oflag = 0;

	/* set input mode (non-canonical, no echo,...) */
	newtio.c_lflag = 0;

	newtio.c_cc[VTIME] = 50; /* inter-character timer unused */
	newtio.c_cc[VMIN] = 1; /* blocking read until 1 chars received */

	tcflush(fd, TCIFLUSH);
	tcsetattr(fd, TCSANOW, &newtio);
	return fd;
}

void Set_LineMode(int fd, int linemode)
{
	struct termios newtio;
	tcgetattr(fd, &newtio); /* save current port settings */
	if (linemode) {
		newtio.c_lflag |= ICANON;
	} else {
		newtio.c_lflag &= (~ICANON);
	}
	tcflush(fd, TCIFLUSH);
	tcsetattr(fd, TCSANOW, &newtio);
}

int TCP_Server(uint16_t port)
{
	int server_socket;
	struct sockaddr_in server_settings;
	int yes = 1;

	memset(&server_settings, 0, sizeof(server_settings));
	server_settings.sin_family = AF_INET;
	server_settings.sin_addr.s_addr = INADDR_ANY;
	server_settings.sin_port = htons(port);

	server_socket = socket(PF_INET, SOCK_STREAM, 0);

	// lose the pesky "Address already in use" error message
	if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof yes) == -1) {
		perror("setsockopt");
		return -1;
	}

	if (bind(server_socket, (struct sockaddr*) &server_settings, sizeof(server_settings)) != 0) {
		perror("bind");
		return -1;
	}

	if (listen(server_socket, 5) != 0) {
		perror("listen");
		return -1;
	}

	return server_socket;
}

int TCP_Connect(char * const server, unsigned int port)
{
	int sockfd;

	struct addrinfo hints, *servinfo, *p;
	int rv;
	char s[INET6_ADDRSTRLEN];
	char cport[10];

	snprintf(cport, sizeof(cport), "%u", port);

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((rv = getaddrinfo(server, port, &hints, &servinfo)) != 0) {
		Warning("getaddrinfo: ");
		Warning(gai_strerror(rv));
		return -1;
	}

	// loop through all the results and connect to the first we can
	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			Warning("client: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			Warning("client: connect");
			continue;
		}

		break;
	}

	if (p == NULL) {
		Warning("client: failed to connect");
		return -1;
	}

	inet_ntop(p->ai_family, get_in_addr((struct sockaddr *) p->ai_addr), s, sizeof s);
	Info("client: connecting to: ");
	Info(s);
	freeaddrinfo(servinfo); // all done with this structure
	return sockfd;
}

int Select_Simplified(struct ReadDataCallback * callbacks, int n)
{
	int ret = 1;
	fd_set readfds;
	char buf[1024];
	int maxfd = -1;
	FD_ZERO(&readfds);
	for (int i = 0; i < n; i++) {
		FD_SET(callbacks[i].fd, &readfds);
		if (maxfd < callbacks[i].fd)
			maxfd = callbacks[i].fd;
	}
	select(maxfd + 1, &readfds, NULL, NULL, NULL);
	for (int i = 0; i < n; i++) {
		if (FD_ISSET(callbacks[i].fd, &readfds)) {
			callbacks[i].callback(callbacks[i].fd, callbacks[i].arg);
		}
	}
	return ret;
}

int UDP_Server(char * port)
{
	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	int rv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC; // set to AF_INET to force IPv4
	hints.ai_socktype = SOCK_DGRAM;
	hints.ai_flags = AI_PASSIVE; // use my IP

	if ((rv = getaddrinfo(NULL, port, &hints, &servinfo)) != 0) {
		Warning("getaddrinfo: ");
		Warning(gai_strerror(rv));
		return -1;
	}

	// loop through all the results and bind to the first we can
	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			Warning("listener: socket");
			continue;
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			Warning("listener: bind");
			continue;
		}

		break;
	}

	if (p == NULL) {
		Warning("listener: failed to bind socket");
		return -1;
	}

	freeaddrinfo(servinfo);

	return sockfd;
}

int UDP_Connect(char * server, char * port)
{
	int sockfd;
	struct addrinfo hints, *servinfo, *p;
	int rv;

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_DGRAM;

	if ((rv = getaddrinfo(server, port, &hints, &servinfo)) != 0) {

		Warning("getaddrinfo: ");
		Warning(gai_strerror(rv));
		return -1;
	}

	// loop through all the results and make a socket
	for (p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
			Warning("talker: socket");
			continue;
		}

		break;
	}

	if (p == NULL) {
		Warning("talker: failed to create socket.");
		return -1;
	}

	freeaddrinfo(servinfo);

	return sockfd;
}

static void sig_pipe(int signo)
{
	Warning("SIGPIPE caught");
}

int Pipe_Create(char *cmd, int fds[2])
{
	int fd1[2], fd2[2];
	pid_t pid;

	if (signal(SIGPIPE, sig_pipe) == SIG_ERR)
		Warning("signal error");
	if (pipe(fd1) < 0 || pipe(fd2) < 0)
		Warning("pipe error");

	pid = fork();

	if (pid < 0) {
		Warning("fork error");
	} else if (pid == 0) {
		/* child */
		close(fd1[1]);
		close(fd2[0]);
		if (fd1[0] != STDIN_FILENO) {
			if (dup2(fd1[0], STDIN_FILENO) != STDIN_FILENO) {
				Warning("dup2 error to stdin");
				return -1;
			}
			close(fd1[0]);
		}
		if (fd2[1] != STDOUT_FILENO) {
			if (dup2(fd2[1], STDOUT_FILENO) != STDOUT_FILENO) {
				Warning("dup2 error to stdout");
			}
			close(fd2[1]);
		}
		if (execl(cmd, "", (char *) 0) < 0) {
			Warning("execl error");
			return -1;
		}

	} else {
		close(fd1[0]);
		close(fd2[1]);
		fds[0] = fd2[0];
		fds[1] = fd1[1];
	}
	return 0;
}
