#include "clib_priv.h"
#include <assert.h>

#include <stdio.h>
#include <unistd.h>

char * String_Join(List * list, char token)
{
	assert(list);
// Check that we don't get NULL in
	if (!list)
		return NULL;

	char * ret; // string to return

	size_t n = 0; // for calculating size of return string
	for (Node *it = list->first; it != NULL; it = it->next) {
		n += it->size; // sum size all strings in list
	}
	ret = mymalloc(n); // allocate memory for the whole new string

	n = 0; // for calculating where to put each string into new string
	for (Node *it = list->first; it != NULL; it = it->next) {
		memcpy(&ret[n], it->data, it->size); // copy string
		ret[n + it->size - 1] = token; // insert token
		n += it->size; // for determining next string
	}
	ret[n - 1] = '\0'; // add zero terminator
	return ret;
}


char * String_Merge(List * list)
{
// check that list != NULL
	assert(list);
	if (!list)
		return NULL;
	char * ret; // return string
	size_t n = 0; // for calculating size of return string
	for (Node *it = list->first; it != NULL; it = it->next) {
		n += it->size - 1; // sum size all strings in list, ignore zero terminators
	}
	ret = mymalloc(n + 1); // allocate memory for new string

	n = 0; // merge together all strings
	for (Node *it = list->first; it != NULL; it = it->next) {
		memcpy(&ret[n], it->data, it->size - 1); // (-1) to skip zero terminators
		n += it->size - 1;
	}
	ret[n] = '\0'; // add zero terminator at end of final string
	return ret;
}

char * String_ReadLine(void)
{
	/*int c;
	Array * tmp = Array_Create(256); // initial buffer with room for upto 256 byte long lines
	char * ret;
	char b;

// read data until a newline is found
	while ((c = getchar()) != '\n') {
		b = (char) c;
		Array_Add(tmp, (unsigned char*) &b, 1); // add every non newline character to automatically growing array
	}
	b = 0; // create zero terminator
	Array_Add(tmp, (char*) &b, 1); // add zero terminator
	ret = strdup((char*) tmp->data); // duplicate string, since we aren't returning it as array
	assert(ret);
	Array_Dispose(tmp); // dispose the array
	return ret;*/
	return String_ReadLinef(stdin, '\n');
}

char * String_ReadLinef(FILE * f, char delim)
{
	int c;
	Array * tmp = Array_Create(256); // initial buffer with room for upto 256 byte long lines
	char * ret;
	char b;

// read data until a newline is found
	while ((c = getc(f)) != delim) {
		b = (char) c;
		Array_Add(tmp, (unsigned char*) &b, 1); // add every non newline character to automatically growing array
	}
	b = 0; // create zero terminator
	Array_Add(tmp, (char*) &b, 1); // add zero terminator
	ret = strdup((char*) tmp->data); // duplicate string, since we aren't returning it as array
	assert(ret);
	Array_Dispose(tmp); // dispose the array
	return ret;
}

int fdgetc(int fd) {
	char c;
	ssize_t n = read(fd, &c, 1);
	if(n == 0) return -1;
	else return c;
}

char * String_ReadLinefd(int fd, char delim)
{
	int c;
	Array * tmp = Array_Create(256); // initial buffer with room for upto 256 byte long lines
	char * ret;
	char b;

// read data until a newline is found
	while ((c = fdgetc(fd)) != delim) {
		b = (char) c;
		Array_Add(tmp, (unsigned char*) &b, 1); // add every non newline character to automatically growing array
	}
	b = 0; // create zero terminator
	Array_Add(tmp, (char*) &b, 1); // add zero terminator
	ret = strdup((char*) tmp->data); // duplicate string, since we aren't returning it as array
	assert(ret);
	Array_Dispose(tmp); // dispose the array
	return ret;
}

List * String_Split(char * str, char token)
{
	assert(str);
// validate input
	if (!str)
		return NULL;
	List * list = List_Create(); // create new list
	size_t i = 0; // for iterating through input string
	size_t start = 0; // start of a substring
	while (str[i] != '\0') { // as long as not zero
		if (str[i] == token) { // if token is found, construct a new Node containing the string
			Node * node = Node_FromBlock(&str[start],
					i - start + 1);
			node->data[i - start] = '\0'; // replace token with zero terminator
			List_AddLast(list, node); // add at the end of the list
			start = i + 1; // point start to right after token
		}
		i++;
	}
	Node * node = Node_FromBlock(&str[start], i - start + 1); // add the last string
	node->data[i - start] = '\0'; // zero terminator
	List_AddLast(list, node); // add node
	start = i + 1;

	return list;
}

char * String_Substring(const char * str, size_t idx, size_t n)
{
	if(str == NULL) {
		Warning("String_Substring: Called with str == NULL.");
	}
	if(idx+n >= strlen(str)) {
		Warning("String_Substring: Trying to take substring out of bounds");
		return NULL;
	}
	char * substr = mymalloc(n+1);
	memcpy(substr, &str[idx], n);
	substr[n] = '\0';
	return substr;
}
