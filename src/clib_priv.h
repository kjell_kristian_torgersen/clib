/*
 * clib_priv.h
 *
 *  Created on: 8. aug. 2017
 *      Author: kjellkr
 */

#ifndef CLIB_PRIV_H_
#define CLIB_PRIV_H_


#include <stdlib.h>
#include <string.h>

#include "clib.h"



struct Iteratorvtable {
	int (*HasNext)(void*);
	void*(*Next)(void*);
	void*(*Get)(void*);
	void(*Destroy)(void*);
};

struct IteratorTag {
	struct Iteratorvtable * vtable;
	void * state;
};

void *mymalloc(size_t __size);

#endif /* CLIB_PRIV_H_ */
