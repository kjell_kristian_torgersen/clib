#include "clib_priv.h"
#include <sys/timerfd.h>
#include <unistd.h>
#include <fcntl.h>

int Timer_Create(void)
{
	// TFD_NONBLOCK
	return timerfd_create(CLOCK_MONOTONIC, O_CLOEXEC);
}

int Timer_RunInterval(int fd, __time_t seconds, __syscall_slong_t nanoseconds)
{
	struct itimerspec timer_settings;
	memset(&timer_settings, 0, sizeof(struct itimerspec));
	timer_settings.it_interval.tv_sec = seconds;
	timer_settings.it_interval.tv_nsec = nanoseconds;
	timer_settings.it_value.tv_sec = seconds;
	timer_settings.it_value.tv_nsec = nanoseconds;

	return timerfd_settime(fd, 0, &timer_settings, NULL);
}

int Timer_RunOnce(int fd, __time_t seconds, __syscall_slong_t nanoseconds)
{
	struct itimerspec timer_settings;
	memset(&timer_settings, 0, sizeof(struct itimerspec));
	timer_settings.it_interval.tv_sec = 0;
	timer_settings.it_interval.tv_nsec = 0;
	timer_settings.it_value.tv_sec = seconds;
	timer_settings.it_value.tv_nsec = nanoseconds;

	return timerfd_settime(fd, 0, &timer_settings, NULL);
}
