#include "clib_priv.h"
#include <assert.h>

Array * Array_Create(size_t size)
{
	Array * ret = mymalloc(sizeof(Array));
	ret->size = 0;
	ret->capacity = size;
	ret->data = mymalloc(ret->capacity);
	return ret;
}

void Array_Add(Array * array, void * data, size_t n)
{
	assert(array);
	assert(data);
	if ((!array) || (!data))
		return;

	// calculate how big the array will be after adding * data
	size_t newsize = array->size + n;

	// determine if we need to grow the array
	if (newsize > array->capacity) {
		// yes, calculate new size
		size_t newcapacity = 2 * newsize;
		array->data = realloc(array->data, newcapacity); // expand data block
		assert(array->data != NULL);
		memcpy(array->data + array->size, data, n); // copy in new data
		array->capacity = newcapacity; // update capacity
		array->size = newsize; // update size
	} else {
		// no, enough capacity to add more data
		memcpy(array->data + array->size, data, n); // copy in new data
		array->size += n; // update size
	}
}

void Array_AddString(Array * array, char * str)
{
	Array_Add(array, str, strlen(str));
}

void Array_Erase(Array * array, size_t idx, size_t n)
{
	assert(array);
	assert(idx < array->size);
	assert(idx + n <= array->size);

	if (!array) {
		Warning("Array_Erase: array == NULL");
		return;
	}
	if (idx >= array->size) {
		Warning("Array_Erase: array out of bounds");
		return;
	}
	if (idx + n > array->size) {
		Warning("Array_Erase: array out of bounds");
		return;
	}

	memmove(array->data + idx, array->data + idx + n, array->size - n);
	array->size -= n;
}

void * Array_Pop(Array * array, size_t n)
{
	array->size -= n;
	return array->data + array->size;
}

void Array_Dispose(Array * array)
{
	if (array) {
		free(array->data); // delete its data
		free(array); // delete its metadata
	}
}
