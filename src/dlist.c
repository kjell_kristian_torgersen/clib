#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct dnode dnode;
typedef struct dlist dlist;

struct dnode {
	struct dnode * prev;
	struct dnode * next;
	char data[];
};

struct dlist {
	struct dnode * head;
	struct dnode * last;
	size_t elemsize;
};

dlist* dlist_create(size_t elemsize)
{
	dlist * ret = malloc(sizeof(dlist));
	ret->elemsize = elemsize;
	ret->head = NULL;
	ret->last = NULL;
	return ret;
}

void dlist_addfront(dlist * list, void* data)
{
	dnode * newnode = malloc(list->elemsize + sizeof(dnode));
	newnode->next = list->head;
	newnode->prev = NULL;
	list->head = newnode;
}

void dlist_addback(dlist * list, void* data)
{
	dnode * newnode = malloc(list->elemsize + sizeof(dnode));
	newnode->next = NULL;
	newnode->prev = list->last;
	list->last = newnode;
}

void dlist_insert(dlist * list, dnode * where, void* data)
{
	dnode * newnode = malloc(list->elemsize + sizeof(dnode));
	newnode->next = where;
	newnode->prev = where->prev;
	newnode->prev->next = newnode;
	newnode->next->prev = newnode;
}

void dlist_remove(dlist* list, dnode * where)
{
	//where->prev->next = where->next;
}

void dlist_destroy(dlist * list)
{
	dnode * prev = NULL;
	for (dnode * it = list->head; it != NULL; it = it->next) {
		free(prev);
		prev = it;
	}
	free(prev);
	free(list);
}
