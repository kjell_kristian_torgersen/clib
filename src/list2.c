#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "clib_priv.h"
#include "list2.h"

struct node {
	struct node * next;
	char data[];
};

struct list {
	struct node * first;
	struct node * last;
	size_t elemsize;
};

typedef struct node iterator;
typedef struct node node;
typedef struct list list;

void it_next(iterator** it)
{
	(*it) = (*it)->next;
}

int it_hasnext(iterator** it)
{
	return (*it) != NULL;
}

void* it_get(iterator** it)
{
	return (*it)->data;
}

iterator* list_getiterator(list* list)
{
	return list->first;
}

list* list_create(size_t elemsize)
{
	list* list = mymalloc(sizeof(struct list));

	list->elemsize = elemsize;
	list->first = NULL;
	list->last = NULL;

	return list;
}

list* list_copy(list* list)
{
	struct list * copy = list_create(list->elemsize);
	for (iterator * it = list->first; it != NULL; it = it->next) {
		list_addback(copy, it->data);
	}
	return copy;
}

list* list_reverse(list* list)
{
	struct list * copy = list_create(list->elemsize);
	for (iterator * it = list->first; it != NULL; it = it->next) {
		list_addfront(copy, it->data);
	}
	return copy;
}

size_t list_elemsize(list* list)
{
	if (list)
		return list->elemsize;
	else
		return 0;
}

size_t list_count(list* list)
{
	size_t count = 0;
	for (iterator* it = list->first; it != NULL; it = it->next) {
		count++;
	}
	return count;
}

void list_foreach1(list* list, foreach1_fp fp)
{
	for (iterator * it = list->first; it != NULL; it = it->next) {
		fp(it->data);
	}
}

void list_foreach2(list* list, foreach2_fp fp, void* arg)
{
	for (iterator * it = list->first; it != NULL; it = it->next) {
		fp(it->data, arg);
	}
}

void list_addfront(list* list, void * data)
{
	if (list == NULL)
		return;

	node * node = mymalloc(list->elemsize + sizeof(struct node));

	if (data != NULL) {
		memcpy(node->data, data, list->elemsize);
	}

	if (list->first == NULL) {
		node->next = NULL;
		list->first = node;
		list->last = node;
	} else {
		node->next = list->first;
		list->first = node;
	}

}

void list_addback(list* list, void * data)
{
	if (list == NULL)
		return;

	node * node = mymalloc(list->elemsize + sizeof(struct node));

	node->next = NULL;
	if (data != NULL) {
		memcpy(node->data, data, list->elemsize);
	}
	if (list->first == NULL) {
		list->first = node;
		list->last = node;
	} else {
		list->last->next = node;
		list->last = node;
	}
}

iterator * list_idx(list* list, int idx)
{
	int i = 0;
	for (iterator* it = list->first; it != NULL; it = it->next) {
		if (i == idx)
			return it;
		i++;
	}

	return NULL;
}

void * list_get(list * list, int idx)
{
	int i = 0;
	for (iterator* it = list->first; it != NULL; it = it->next) {
		if (i == idx)
			return it->data;
		i++;
	}

	return NULL;
}

void list_destroy(list* list)
{
	if (list == NULL)
		return;
	iterator * prev = NULL;
	for (iterator * it = list->first; it != NULL; it = it->next) {
		free(prev);
		prev = it;
	}
	free(prev);
	free(list);
}

void list_insertafter(list* list, iterator* where, void* data)
{
	if (where == NULL)
		return;
	node * node = mymalloc(list->elemsize + sizeof(struct node));
	node->next = where->next;
	if (data != NULL)
		memcpy(node->data, data, list->elemsize);
	where->next = node;
}

void list_insertbefore(list* list, iterator* where, void* data)
{
	if (where == NULL)
		return;
	node * node = mymalloc(list->elemsize + sizeof(struct node));
	node->next = where->next;
	memcpy(node->data, where->data, list->elemsize);
	if (data != NULL)
		memcpy(where->data, data, list->elemsize);
	where->next = node;
}

void list_remove(list* list, iterator* where)
{
	if (where == list->first) {
		list->first = list->first->next;
		free(where);
	} else {
		iterator* prev = NULL;
		for (iterator* it = list->first; it != where; it = it->next) {
			prev = it;
		}
		prev->next = where->next;
		free(where);
	}
}

iterator* list_find(list* list, compare_fp fp, void* arg)
{
	for (iterator* it = list->first; it != NULL; it = it->next) {
		if (fp(it->data, arg) == 0) {
			return it;
		}
	}
	return NULL;
}

int list_findremove(list* list, compare_fp fp, void* arg)
{
	iterator* prev = NULL;
	for (iterator* it = list->first; it != NULL; it = it->next) {
		if (fp(it->data, arg) == 0) {
			if (prev == NULL) {
				list->first = list->first->next;
				free(it);
				return 1;
			} else {
				prev->next = it->next;
				free(it);
				return 1;
			}
		}
		prev = it;
	}
	return 0;
}

list* list_filter1(list* list, filter1_fp fp)
{
	struct list* ret = list_create(list->elemsize);
	for (iterator* it = list->first; it != NULL; it = it->next) {
		if (fp(it->data)) {
			list_addback(ret, it->data);
		}
	}
	return ret;
}

list* list_filter2(list* list, filter2_fp fp, void* arg)
{
	struct list* ret = list_create(list->elemsize);
	for (iterator * it = list->first; it != NULL; it = it->next) {
		if (fp(it->data, arg)) {
			list_addback(ret, it->data);
		}
	}
	return ret;
}

static node* merge(node* left, node* right, compare_fp fp)
{
	node* result = NULL;
	node* last = NULL;

	while (left && right) {
		if (fp(left->data, right->data) <= 0) {
			if (result) {
				last->next = left;
				last = last->next;
			} else {
				result = last = left;
			}
			left = left->next;
		} else {
			if (result) {
				last->next = right;
				last = last->next;
			} else {
				result = last = right;
			}
			right = right->next;
		}
	}

	// Either left or right may have elements left; consume them.
	// (Only one of the following loops will actually be entered.)
	while (left) {
		if (result) {
			last->next = left;
			last = last->next;
		} else {
			result = last = left;
		}
		left = left->next;
	}
	while (right) {
		if (result) {
			last->next = right;
			last = last->next;
		} else {
			result = last = right;
		}
		right = right->next;
	}
	return result;
}

void list_mergesort(list* list, compare_fp fp)
{
	if (list == NULL)
		return;
	if (list->first == NULL)
		return;

	node * array[32] = { 0 };

#define N (sizeof(array)/sizeof(array[0]))

	node * result = NULL;
	node * next = NULL;
	size_t i = 0;

	result = list->first;
	while (result != NULL) {
		next = result->next;
		result->next = NULL;
		for (i = 0; (i < N) && (array[i] != NULL); i++) {
			result = merge(array[i], result, fp);
			array[i] = NULL;
		}
		if (i == N)
			i--;
		array[i] = result;
		result = next;
	}
	result = NULL;
	for (i = 0; i < N; i++) {
		result = merge(array[i], result, fp);
	}
#undef N
	list->first = result;
	for (iterator * it = list->first; it != NULL; it = it->next) {
		list->last = it;
	}
}

