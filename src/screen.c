#include "clib_priv.h"
#include <stdio.h>

void Screen_Clear()
{
	printf("\x1b[2J");
	fflush(stdout);
}


void Screen_ColorF(int n)
{
	if(n < 8) {
		printf("\x1b[3%cm", '0' + n);
	} else {
		printf("\x1b[9%cm", '0' + n-8);
	}
	fflush(stdout);
}

void Screen_ColorB(int n)
{
	if(n < 8) {
		printf("\x1b[4%cm", '0' + n);
	} else {
		printf("\x1b[10%cm", '0' + n-8);
	}
	fflush(stdout);
}

void Screen_Color(int f, int b) {
	Screen_ColorF(f);
	Screen_ColorB(b);
}

void Screen_Locate(int x, int y)
{
	printf("\x1b[%i;%if", y + 1, x + 1);
	fflush(stdout);
}
