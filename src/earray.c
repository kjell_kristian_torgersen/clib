#include "clib_priv.h"
#include <assert.h>
#include <stdio.h>

typedef struct EArrayTag {
	size_t size;
	size_t elemsize;
	char * data;
} EArray;

static int EAIt_HasNext(void * state);
static void * EAIt_Get(void * state);
static void * EAIt_Next(void * state);
static void EAIt_Destroy(void * state);

static struct Iteratorvtable eavtable = { EAIt_HasNext, EAIt_Next, EAIt_Get, EAIt_Destroy };
typedef struct eaitstate {
	EArray * array;
	char * next;
} eaitstate_t;

Iterator * EArray_GetIterator(EArray * arr)
{
	Iterator *it = mymalloc(sizeof(Iterator));
	eaitstate_t * state = mymalloc(sizeof(eaitstate_t));

	state->array = arr;
	state->next = arr->data;

	it->vtable = &eavtable;
	it->state = state;

	return it;
}

static int EAIt_HasNext(void * state)
{
	eaitstate_t * e = state;
	return e->next != e->array->data + e->array->elemsize * e->array->size;
}

static void* EAIt_Get(void * state)
{
	eaitstate_t * e = state;
	return e->next;
}

static void * EAIt_Next(void * state)
{
	eaitstate_t * e = state;
	e->next += e->array->elemsize;
	return state;
}

static void EAIt_Destroy(void * state)
{
	free(state);
}

EArray * EArray_Create(size_t elemsize)
{
	EArray * array = (EArray*) mymalloc(sizeof(EArray));
	array->size = 0;
	array->elemsize = elemsize;
	array->data = NULL;
	return array;
}

void * EArray_Get(EArray * array, size_t idx)
{
	assert(idx < array->size);
	if (idx >= array->size) {
		Warning("Trying to read out of array boundaries.");
		fprintf(stderr, "Warning:  Array out of bounds read: %lu of %lu\n", idx, array->size);
	}
	return array->data + array->elemsize * idx;
}

void EArray_Set(EArray * array, size_t idx, void * data)
{
	assert(idx < array->size);
	if (idx >= array->size) {
		Warning("Trying to write out of array boundaries.");
		fprintf(stderr, "Warning: Array out of bounds write: %lu of %lu\n", idx, array->size);
	} else {
		memcpy(array->data + array->elemsize * idx, data, array->elemsize);
	}
}

size_t EArray_Size(EArray * array)
{
	return array->size;
}

size_t EArray_ElemSize(EArray * array)
{
	return array->elemsize;
}

void EArray_Resize(EArray * array, size_t new_size)
{
	array->data = realloc(array->data, sizeof(EArray) + new_size * array->elemsize);
	array->size = new_size;
}

void EArray_Dispose(EArray * array)
{
	if (array) {
		if (array->data)
			free(array->data);
		free(array);
	}
}
