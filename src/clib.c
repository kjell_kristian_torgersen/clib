#include "clib_priv.h"
#include <assert.h>
#include <stdio.h>

static const char * logfilename = NULL;
void Log_Start(const char * filename)
{
	logfilename = filename;
	Info("Logging started.");
}

void Log_Stop(void)
{
	Info("Logging stopped.");
	logfilename = NULL;
}
void *mymalloc(size_t __size)
{
	void * tmp = malloc(__size);
	assert(tmp != NULL);
	if (tmp == NULL) {
		Warning("Error allocating memory.");
	}
	return tmp;
}
void writelogfile(char * type, const char * message)
{
	if (logfilename) {
		FILE * log = fopen(logfilename, "a");
		if (log) {
			time_t t = time(NULL);
			struct tm tm = *localtime(&t);
			fprintf(log, "%0.2d-%0.2d-%0.2d %0.2d:%0.2d:%0.2d: %s: %s\n", tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec, type, message);
			fflush(log);
			fclose(log);
		}
	}
}
void Error(const char *message)
{
	writelogfile("Error", message);
	fprintf(stderr, "Error: %s\n", message);
	exit(0);
}

void Warning(const char *message)
{
	writelogfile("Warning", message);
	fprintf(stderr, "Warning: %s\n", message);
}

void Info(const char *message)
{
	if (logfilename) {
		writelogfile("Info", message);
	} else {
		printf("Info: %s\n", message);
	}
}
