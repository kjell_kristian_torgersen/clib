#ifndef CLIB_H_
#define CLIB_H_

#include <stdio.h>
#include <stdint.h>
#include <termios.h>
#include <netinet/in.h>
#include <pthread.h>
//#include <unistd.h>

typedef int(*filter1_fp)(void*);
typedef int(*filter2_fp)(void*,void*);
typedef void(*foreach1_fp)(void*);
typedef void(*foreach2_fp)(void*,void*);
typedef int(*compare_fp)(void*,void*);

typedef struct IteratorTag Iterator;
typedef struct EArrayTag EArray;

typedef struct ArrayTag {
	size_t capacity; ///< don't alter this value
	size_t size; ///< must be between 0 and (capacity-1)
	unsigned char * data; ///< never change this pointer, but you are free to alter data[0]-data[capacity-1]
} Array;


typedef struct NodeTag {
	struct NodeTag * next; ///< don't alter this value
	size_t size; ///< don't alter this value
	unsigned char data[]; //alter data[0]-data[size-1] freely
} Node;

typedef struct ListTag {
	Node * first; ///< don't alter this value
	Node * last; ///< don't alter this value
} List;

struct StackTag {
	struct StackTag * below;
	unsigned char data[];
};

struct ReadDataCallback {
	int fd;
	void (*callback)(int fd, void * arg);
	void * arg;
};

typedef struct StackTag* Stack;

typedef int (*cmp_ptr)(const void *, const void *);
typedef void (*apply_ptr)(void*);
typedef int (*predicate_ptr)(void* data, void* state);
typedef char * string;

/** Create a (dynamically growing) array. Remember calling Array_Dispose() on this array when finished using it.
* \param size Initial capacity in bytes */
Array* Array_Create(size_t size);
/** \brief Add a block of data to the (end of) array (specified by array->size). Automatically grow array if necessary. */
void Array_Add(Array * array, void * data, size_t n);
/** \brief Add a zero-terminated string to the (end of) the array. */
void Array_AddString(Array * array, char * str);
/** \brief Erase n bytes starting at idx from the array. */
void Array_Erase(Array * array, size_t idx, size_t n);
/** \brief Pop off n bytes from the array. The returned pointers data is automatically freed when the specified array later is disposed. */
void * Array_Pop(Array * array, size_t n);
/** \brief Dispose an array (and its data) */
void Array_Dispose(Array * array);

EArray * EArray_Create(size_t elemsize);
void * EArray_Get(EArray * array, size_t idx);
void EArray_Set(EArray * array, size_t idx, void * data);
size_t EArray_Size(EArray * array);
size_t EArray_ElemSize(EArray * array);
void EArray_Resize(EArray * array, size_t new_size);
void EArray_Dispose(EArray * array);
Iterator * EArray_GetIterator(EArray * arr);

/** \brief Create a new empty singly linked list. Remember call List_Dispose() on this list when done using it. */
List* List_Create(void);
/** \brief Sequentially call fp(n->data, state) for each node n in list. state belongs to fp(), and fp() may manipulate it freely (to keep whatever state it needs).
 * \param *list List to traverse
 * \param *fp Function pointer to call on all elements
 * \param *state Initial state used together with *fp
 */
void List_Fold(List * list, void (*fp)(void*data, void*state), void * state);
/** \brief Sequentially call fp(n->data) for each node n in list */
void List_Apply(List * list, void (*fp)(void*));
/** \brief Add an element to the beginning of the list. node (and its content) is automatically disposed when list is disposed. */
void List_AddFirst(List * list, Node * node);

void List_AddSort(List * list, Node * node,
		int (*cmp)(const void *, const void *));
/** \brief Add an element to the end of the list. node (and its content) is automatically disposed when list is disposed.  */
void List_AddLast(List * list, Node * node);
/** \brief Create a new list that excludes all elements where predicate returns 0.
 * \param *list Input list
 * \param *predicate, function that inspects n->data for node n and return 0 if this node should be excluded. */

List * List_Filter(List * list, int (*predicate)(void* data, void* state),
		void* state);
/** \brief Dispose a list. */
void List_Dispose(List * list);
/** Go through *list, add elements to *a if fp() returns true, else add to *b  */
void List_Partition(List * list, List * a, List * b,
		int (*fp)(void*data, void* state), void * state);
/** Create a new list consisting of a, then b. Remember to call List_Dispose() on returned value when done using it. */
List* List_Join(List * a, List * b);
/** \brief Copy a list. Remember to call List_Dispose() on this list when done using it.
 * \return A deep (also data are copied) copy of *list. */
List* List_Duplicate(List * original);
/** \brief Reverse a list. Remember to call List_Dispose() on this list when done using it.
 * \return A deep (also data are copied) copy of *list in reverse order. */
List* List_Reverse(List * list);
/** \brief Convert from a linked list to an array. Remember calling Array_Dispose() on this array when done using it. */
Array* List_ToArray(List * list);

Iterator * List_GetIterator(List * l);

/** \brief Create a new node of a given size to the linked list*/
Node* Node_Create(size_t size);
/** \brief Make a copy of an existing node. */
Node* Node_FromNode(Node * node);
/** \brief Create a node from a zero terminated string */
Node* Node_FromString(string str);
/** \brief Create a node from a block of data, n bytes */
Node* Node_FromBlock(void * data, size_t n);
/** \brief Insert a node after target*/
void Node_InsertAfter(Node * target, Node * node);

/** Join all node data with a given token between. Use free() on this string when finished using it. */
string String_Join(List * list, char token);
/** \brief Merge all strings in list to one long string. Use free() on this string when finised using it.*/
string String_Merge(List * list);
/** \brief Read a string from default input.
 * \return pointer to a zero terminated string. Remember to call free() on this pointer when your done using it.  */
string String_ReadLine(void);
/** \brief Read a string from specified input.
 * \param *f FILE pointer to read from
 * \param delim Which character to stop at
 * \return pointer to a zero terminated string. Remember to call free() on this pointer when your done using it.  */
char * String_ReadLinef(FILE * f, char delim);
/** \brief Read a string from a file descriptor
 * \param fd File descriptor to read from
 * \param delim Which character to stop at
 * \return pointer to a zero terminated string. Remember to call free() on this pointer when your done using it.  */
char * String_ReadLinef(FILE * f, char delim);
/** \brief Split a string into a list with the given token. Use List_Dispose() on this list when finished using it. */
List* String_Split(string str, char token);
/** \brief Take a substring of str, starting at idx and taking n characters.
 * \return Substring. Remember to call free() on this when done using it. */
char * String_Substring(const char * str, size_t idx, size_t n);
/** \brief Read all bytes specified in *filename and return as array. Use Array_Dispose() on this array when done using it. \warning Don't use on to big files.*/
Array* File_ReadAllBytes(char * filename);
/** \brief Write the content of the array bytes to a file. Replaces file if existing. */
void File_WriteAllBytes(char * filename, Array * bytes);
/** \brief Read all lines in a text file and return as a linked List containing them. Use List_Dispose() on this list when done using it.
 * \return linked List on success, NULL on failure. */
List * File_ReadAllLines(char * filename, char delim);
/** \brief Write all lines to a the text file specified with *filename. If the file already exists, it will be overwritten without any warning. */
void File_WriteAllLines(char * filename, List * lines, char delim);
/** \brief Append a zero terminated string to file specified with filename. */
void File_AppendLine(char *filename, char *line);
/** \brief Connect to a serial port and obtain a file descritor. Don't forget to close the descriptor with the close() system call when done with it.
 * \return File descriptor. */
int Serialport_Connect(char * device, tcflag_t baud, struct termios * oldtio);
/** \brief Set canon mode on the specified file descriptor. If set, then input will only appear available when a newline character is encountered.
 * \param fd File descriptor (serial port, socket...)
 * \param linemode 0 to turn off, else turn on. */
void Set_LineMode(int fd, int linemode);
/** \brief Start a TCP server on specified port. Don't forget to close the descriptor with the close() system call when done with it.
 * \return Socket file descriptor. */
int TCP_Server(uint16_t port);
/** \brief Connect to specified TCP server on specified port. Don't forget to close the descriptor with the close() system call when done with it.
 * \return Socket file descriptor. */
int TCP_Connect(char * const server, unsigned int port);
/** \brief Start a UDP server on specified port. Don't forget to close the descriptor with the close() system call when done with it.
 * \return Socket file descriptor. */
int UDP_Server(char * port);
/** \brief Perform the select system call, with n file descriptors and callbacks specified in callbacks. */
int Select_Simplified(struct ReadDataCallback * callbacks, int n);
/** \brief Connect to specified UDP server on specified port. Don't forget to close the descriptor with the close() system call when done with it.
 * \return Socket file descriptor. */
int UDP_Connect(char * server, char * port);

/** \brief Create a file descriptor based timer. Don't forget to close the descriptor with the close() system call when done with it.
 * \return File descriptor */
int Timer_Create(void);
/** \brief Set up the file descriptor based timer to run interval mode. The file descriptor will have 8 bytes available at timeout. */
int Timer_RunInterval(int fd, __time_t seconds, __syscall_slong_t nanoseconds);
/** \brief Set up the file descriptor based timer to run just once. The file descriptor will have 8 bytes available at timeout.  */
int Timer_RunOnce(int fd, __time_t seconds, __syscall_slong_t nanoseconds);

/** \brief Clear the screen */
void Screen_Clear();
/** \brief Set a color. */
void Screen_ColorF(int n);
void Screen_ColorB(int n);
void Screen_Color(int f, int b);
/** \brief Set cursor position. Starts at 0,0 */
void Screen_Locate(int x, int y);

int It_HasNext( Iterator* it);
void It_Next(Iterator * it);
void* It_Get(Iterator * it);
void It_Dispose(Iterator * it);

pthread_t Thread_Start(void *(*start_routine) (void *), void * argument);
void * Thread_Join(pthread_t th);

Iterator * Range(int start, int stop);
int Pipe_Create(char *cmd, int fds[2]);

void Error(const char *message);
void Warning(const char *message);
void Info(const char *message);
void Log_Start(const char * filename);
void Log_Stop(void);

typedef struct rng rng_t;
int rng_size(void);
/* \brief Create and seed a new random number generator.
 * \param seed Seed for random number generator
 * \return Pointer to random number structure necessary for the other related functions dealing with generation of random numbers. */
rng_t * RNG_Init(int seed);
uint32_t RNG_uint32(rng_t * this);
/* \brief Get a random byte */
uint16_t RNG_uint16(rng_t * this);
/* \brief Get a random byte */
uint8_t RNG_uint8(rng_t * this);
/* \brief Get a random double */
double RNG_Double(rng_t * this);
/* \brief Write n random bytes to buf */
void RNG_Write(rng_t * this, void * buf, int n);

#endif /* CLIB_H_ */
