#ifndef DARRAY_H_
#define DARRAY_H_

#include "clib.h"

typedef struct darray darray;
typedef struct node iterator;

darray * da_create(size_t elemsize);
void da_addback(darray* arr, void* data);
void da_addbackmany(darray* arr, void* data, size_t n);
void da_insert(darray* arr, size_t idx, void* data);
void da_insertmany(darray* arr, size_t idx, void* data, size_t n);
void * da_get(darray* arr, size_t idx);
void da_set(darray* arr, size_t idx, void* data);
size_t da_elemsize(darray* arr);
size_t da_capacity(darray* arr);
size_t da_count(darray* arr);
darray * da_reverse(darray* arr);
void da_foreach1(darray* arr, foreach1_fp fp);
void da_foreach2(darray* arr, void(*fp)(void*,void*), void *arg);
void da_insertionsort(darray * arr, compare_fp fp);
void da_mergesort(darray* arr, compare_fp fp);
darray * da_copy(darray * arr);
void da_quicksort(darray * arr, compare_fp fp);
void da_destroy(darray* arr);


#endif /* DARRAY_H_ */
