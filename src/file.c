#include "clib_priv.h"
#include <assert.h>
#include <stdio.h>

Array * File_ReadAllBytes(char * filename)
{
	assert(filename);
// ensure filename != NULL
	if (!filename)
		return NULL;
// open file
	FILE * f = fopen(filename, "rb");
	if (f) {
		// measure file size
		fseek(f, 0, SEEK_END);
		size_t filesize = (size_t) ftell(f);
		// create a perfectly sized array
		Array * ret = Array_Create(filesize);
		fseek(f, 0, SEEK_SET); // go to beginning of file
		ret->size = fread(ret->data, 1, filesize, f); // fill array with data
		fclose(f); // close
		return ret;
	} else {
		Warning("Error opening file for reading: ");
		Warning(filename);
		return NULL;
	}
}

void File_WriteAllBytes(char * filename, Array * bytes)
{
	assert(filename);
	assert(bytes);
// ensure filename and bytes != NULL
	if ((!filename) || (!bytes))
		return;

// open file for writing
	FILE * f = fopen(filename, "wb");
	assert(f);
	if (f) {
		// if success, write all bytes
		fwrite(bytes->data, 1, bytes->size, f);
		fclose(f);
	} else {
		Warning("Error opening file for writing: ");
		Warning(filename);
	}
}

List * File_ReadAllLines(char * filename, char delim)
{
	assert(filename);
	if (!filename)
		return NULL;
	List * ret = NULL;
	FILE * f = fopen(filename, "rb");
	char b;
	int c;

	// Check if file was opened
	if (f) {
		ret = List_Create();	// create a new list
		Array * buf = Array_Create(256); // automatically dynamically growing buffer for lines

		// Read until End of file
		while ((c = getc(f)) != EOF) {
			b = (char) c;		// convert to byte (from int)

			if (b != '\n') {
				// no newline, add characters to (growing) buffer
				Array_Add(buf, &b, 1);
			} else {
				// newline, add zero terminator and copy string into linked list.
				b = 0; // create zero terminator
				Array_Add(buf, &b, 1); // add zero terminator
				List_AddLast(ret, Node_FromString((char*) buf->data)); // add copy line to end of list
				buf->size = 0; // reset buffer array
			}
		}
		// Delete array used as temporary buffer
		Array_Dispose(buf);
		// Close file
		fclose(f);
	} else {
		Warning("Error opening file for reading: ");
		Warning(filename);
	}

	return ret;
}

void File_WriteAllLines(char * filename, List * lines, char delim)
{
	assert(filename);
	assert(lines);
// ensure that both filename and lines aren't NULL
	if ((!filename) || (!lines))
		return;
// open file
	FILE * f = fopen(filename, "wb");
	assert(f);
	if (f) {
		// if success, iterate through list, write every line
		for (Node * it = lines->first; it != NULL; it = it->next) {
			fprintf(f, "%s%c", (char*) it->data, delim);
		}
		fclose(f);
	} else {
		Warning("Error opening file for writing: ");
		Warning(filename);
	}
}

void File_AppendLine(char *filename, char *line)
{
	assert(filename);
	assert(line);
	if ((!filename) || (!line))
		return;

	FILE * f = fopen(filename, "a");
	if (f) {
		fputs(line, f);
		fclose(f);
	} else {
		Warning("Couldn't open file: ");
		Warning(filename);
	}
}
