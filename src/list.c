#include "clib_priv.h"
#include <assert.h>

static int ListIt_HasNext(void * state);
static void * ListIt_Get(void * state);
static void * ListIt_Next(void * state);
static void ListIt_Destroy(void * state);

static struct Iteratorvtable listvtable = { ListIt_HasNext, ListIt_Next, ListIt_Get, ListIt_Destroy };

Iterator * List_GetIterator(List * l)
{
	Iterator *it = mymalloc(sizeof(Iterator));
	it->vtable = &listvtable;
	it->state = l->first;
	return it;
}

static int ListIt_HasNext(void * state)
{
	return state != NULL;
}

static void* ListIt_Get(void * state)
{
	Node * n = state;
	return n->data;
}

static void * ListIt_Next(void * state)
{
	Node * n = state;
	return n->next;
}

static void ListIt_Destroy(void * state)
{
	(void)state;
}

Node * Node_Create(size_t size)
{
	Node * ret = mymalloc(sizeof(Node) + size); // create a node + size of data
	ret->size = size; // store size
	return ret;
}

Node * Node_FromNode(Node * node)
{
	assert(node);
	if (!node)
		return NULL;
	Node * copy = Node_Create(node->size); // create a new of same size
	copy->next = NULL; // set it to point to NULL (will be updated later)
	copy->size = node->size; // set same size
	memcpy(copy->data, node->data, node->size); // copy all data
	return copy;
}

Node * Node_FromString(char * str)
{
	assert(str);
	if (!str)
		return NULL;

	size_t n = strlen(str) + 1; // determine size including zero terminator
	Node * node = Node_Create(n); // create a node of same size as string
	node->next = NULL; // set next to point to NULL
	node->size = n; // update size
	memcpy(node->data, str, n); // copy string including zero terminator
	return node;
}

Node * Node_FromBlock(void * data, size_t n)
{
	assert(data);
	if (!data)
		return NULL;

	Node * node = Node_Create(n); // create a node of specified size
	node->next = NULL; // point to NULL
	node->size = n; // store size in node
	memcpy(node->data, data, n); // copy block of data
	return node;
}

void Node_InsertAfter(Node * target, Node * node)
{
	assert(target);
	assert(node);
	// Check that target && node != NULL
	if ((!target) || (!node))
		return;

	node->next = target->next;
	target->next = node;
}

List * List_Create(void)
{
	List * list = mymalloc(sizeof(List)); // create the list structure
	list->first = NULL; // first == last == NULL means empty list
	list->last = NULL;
	return list;
}

void List_Fold(List * list, void (*fp)(void*data, void*state), void * state)
{
	assert(list);
	assert(fp);
	if (!list || !fp)
		return;
	for (Node * it = list->first; it != NULL; it = it->next) {
		fp(it->data, state);
	}
}

void List_Apply(List * list, void (*fp)(void*))
{
	assert(list);
	assert(fp);
	if (!list || !fp)
		return;

	for (Node * it = list->first; it != NULL; it = it->next) {
		fp(it->data);
	}
}

void List_AddFirst(List * list, Node * node)
{
	// Check that list && node != NULL
	assert(list);
	assert(node);
	if ((!list) || (!node))
		return;
	// Check if empty list
	if (list->first == NULL) {
		// then first node will also be last node
		list->first = node;
		list->last = node;
	} else {
		node->next = list->first; // add new node before the first node
		list->first = node; // then set the new node as the first node in the list
	}
}

void List_AddSort(List * list, Node * node, int (*cmp)(const void *, const void *))
{
	assert(list);
	assert(node);
	assert(cmp);
	if (!list || !node || !cmp)
		return;
	if (list->first == NULL) {
		List_AddFirst(list, node);
	} else {

		// if before first, add first
		if (cmp(node->data, list->first->data) <= 0) {
			List_AddFirst(list, node);
			return;
		}

		// if after last, add last
		if (cmp(node->data, list->last->data) >= 0) {
			List_AddLast(list, node);
			return;
		}
		Node * prev = NULL;
		for (Node * it = list->first; it != NULL; it = it->next) {
			int r = cmp(it->data, node->data);
			//printf("cmp %s %s = %i\n", it->data, node->data, r);
			if (r >= 0) {
				Node_InsertAfter(prev, node);
				return;
			}
			prev = it;
		}
	}
}

void List_AddLast(List * list, Node * node)
{
	assert(list);
	assert(node);
// Check that list && node != NULL
	if ((!list) || (!node))
		return;

// Check if empty list
	if (list->first == NULL) {
		// then first node will also be last node
		list->first = node;
		list->last = node;
	} else {
		list->last->next = node; // add new node after the last node
		list->last = node; // then set the new node as the last node in the list
	}
}

List * List_Join(List * a, List * b)
{

	assert(a);
	assert(b);
	if (!a && !b)
		return NULL;
	List * ret = List_Create();
	if (a) {
		for (Node * it = a->first; it != NULL; it = it->next) {
			List_AddFirst(ret, Node_FromNode(it));
		}
	}

	if (b) {
		for (Node * it = b->first; it != NULL; it = it->next) {
			List_AddFirst(ret, Node_FromNode(it));
		}
	}
	return ret;
}

List* List_Duplicate(List * list)
{

	// check if original != NULL
	assert(list);
	if (!list)
		return NULL;
	// create a new list
	List * copy = List_Create();

	// Go through existing list, add every node at the beginning of the new list. This effectively constructs a new list that is the reverse of given list.
	for (Node * it = list->first; it != NULL; it = it->next) {
		List_AddLast(copy, Node_FromNode(it));
	}
	return copy;
}

void List_Partition(List * list, List * a, List * b, int (*fp)(void*data, void* state), void * state)
{
	assert(list);
	assert(a);
	assert(b);
	assert(fp);
	if (!list || !a || !b || !fp)
		return;
	for (Node * it = list->first; it != NULL; it = it->next) {
		if (fp(it->data, state)) {
			List_AddLast(a, Node_FromNode(it));
		} else {
			List_AddLast(b, Node_FromNode(it));
		}
	}
}

List * List_Reverse(List * list)
{
	// check if list != NULL
	assert(list);
	if (!list)
		return NULL;
	// create a new list
	List * rev = List_Create();

	// Go through existing list, add every node at the beginning of the new list. This effectively constructs a new list that is the reverse of given list.
	for (Node * it = list->first; it != NULL; it = it->next) {
		List_AddFirst(rev, Node_FromNode(it));
	}
	return rev;
}

Array * List_ToArray(List * list)
{
	assert(list);
	if (!list)
		return NULL;

	Array * array; // Array to create
	size_t n = 0; // for calculating the size
	for (Node * it = list->first; it != NULL; it = it->next) {
		n += it->size;
	}

	array = Array_Create(n); // create an array
	array->size = n;		 // set it as full

	n = 0; // for defining where to copy each entry in the list into the array
	for (Node * it = list->first; it != NULL; it = it->next) {
		memcpy(array->data + n, it->data, it->size); // copy each entry directly into array
		n += it->size; // calculate next spot in array to put data
	}
	return array;
}

List * List_Filter(List * list, int (*predicate)(void* data, void* state), void* state)
{
	assert(list);
	assert(predicate);
	if (!list || !predicate)
		return NULL;
	List * filtered = List_Create();
	for (Node * it = list->first; it != NULL; it = it->next) {
		if (predicate(it->data, state))
			List_AddLast(filtered, Node_FromNode(it));
	}
	return filtered;
}

void List_Dispose(List * list)
{
	assert(list);
	// Check that we don't get NULL in
	if (!list)
		return;

	Node * prev = NULL; // in order for the for loop to work properly, it must be able to iterate to the next node before the previous is deleted
	for (Node * it = list->first; it != NULL; it = it->next) {
		if (prev) // if set, delete
			free(prev);
		prev = it; // let it be deleted next time
	}
	if (prev)
		free(prev); // delete last node

	free(list); // delete the list
}
