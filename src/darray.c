#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include "darray.h"
#include "clib_priv.h"

typedef struct darray {
	size_t count;
	size_t elemsize;
	size_t capacity;
	char * data;
} darray;

size_t da_elemsize(darray* arr)
{
	return arr->elemsize;
}

size_t da_count(darray* arr)
{
	return arr->count;
}

size_t da_capacity(darray* arr)
{
	return arr->capacity;
}

void * da_get(darray* arr, size_t idx)
{
	if (idx < arr->count) {
		return arr->data + idx * arr->elemsize;
	} else {
		fprintf(stderr, "da_get(): Array out of bounds: %lu of %lu\n", idx,
				arr->count);
		return NULL;
	}
}

void da_set(darray* arr, size_t idx, void* data)
{
	if (idx < arr->count) {
		memcpy(arr->data + idx * arr->elemsize, data, arr->elemsize);
	} else {
		fprintf(stderr, "da_set(): Array out of bounds: %lu of %lu\n", idx,
				arr->count);
	}
}

void da_destroy(darray* arr)
{
	free(arr->data);
	free(arr);
}

darray * da_create(size_t elemsize)
{
	darray * ret = mymalloc(sizeof(darray));
	ret->count = 0;
	ret->elemsize = elemsize;
	ret->capacity = 10;
	ret->data = malloc(ret->elemsize * ret->capacity);
	return ret;
}

void da_reserve(darray * arr, size_t new_size)
{
	arr->data = realloc(arr->data, arr->elemsize * new_size);
	arr->capacity = new_size;
	if (arr->count >= arr->capacity)
		arr->count = arr->capacity;
}

void da_insert(darray* arr, size_t idx, void* data)
{
	if (idx <= arr->count) {
		arr->count++;
		if (arr->count > arr->capacity) {
			da_reserve(arr, 2 * arr->count);
		}

		memmove(arr->data + (idx + 1) * arr->elemsize,
				arr->data + idx * arr->elemsize,
				(arr->count - idx) * arr->elemsize);
		memcpy(arr->data + idx * arr->elemsize, data, arr->elemsize);
	} else {
		fprintf(stderr, "da_insert(): Array access our of bounds: %lu of %lu\n",
				idx, arr->count);
	}
}

void da_insertmany(darray* arr, size_t idx, void* data, size_t n)
{
	if (idx <= arr->count) {
		arr->count += n;
		if (arr->count > arr->capacity) {
			da_reserve(arr, 2 * arr->count);
		}

		memmove(arr->data + (idx + n) * arr->elemsize,
				arr->data + idx * arr->elemsize,
				(arr->count - idx) * arr->elemsize);
		memcpy(arr->data + idx * arr->elemsize, data, n * arr->elemsize);
	} else {
		fprintf(stderr, "da_insert(): Array access our of bounds: %lu of %lu\n",
				idx, arr->count);
	}
}

void da_addback(darray* arr, void* data)
{

	if (arr->count + 1 > arr->capacity) {
		da_reserve(arr, 2 * (arr->count + 1));
	}
	memcpy(arr->data + arr->count * arr->elemsize, data, arr->elemsize);
	arr->count++;
}

void da_addbackmany(darray* arr, void* data, size_t n)
{

	if (arr->count + n > arr->capacity) {
		da_reserve(arr, 2 * (arr->count + n));
	}
	memcpy(arr->data + arr->count * arr->elemsize, data, n * arr->elemsize);
	arr->count += n;
}

darray * da_reverse(darray* arr)
{
	darray * rev = da_create(arr->elemsize);
	da_reserve(rev, arr->count);
	rev->count = arr->count;
	for (size_t i = 0; i < arr->count; i++) {
		memcpy(rev->data + i * rev->elemsize,
				arr->data + (arr->count - i - 1) * arr->elemsize,
				arr->elemsize);
	}
	return rev;
}

void da_foreach1(darray* arr, foreach1_fp fp)
{
	for (size_t i = 0; i < arr->count; i++) {
		fp(arr->data + i * arr->elemsize);
	}
}

void da_foreach2(darray* arr, foreach2_fp fp, void *arg)
{
	for (size_t i = 0; i < arr->count; i++) {
		fp(arr->data + i * arr->elemsize, arg);
	}
}

//  Left source half is A[ begin:middle-1].
// Right source half is A[middle:end-1   ].
// Result is            B[ begin:end-1   ].
void merge(darray* A, size_t begin, size_t middle, size_t end, darray *B,
		compare_fp fp)
{
	size_t i = begin, j = middle;
	size_t e = A->elemsize;
	// While there are elements in the left or right runs...
	for (size_t k = begin; k < end; k++) {
		// If left run head exists and is <= existing right run head.
		if (i < middle
				&& (j >= end || fp(A->data + i * e, A->data + j * e) <= 0)) {
			memcpy(B->data + e * k, A->data + e * i, e);
			i = i + 1;
		} else {
			memcpy(B->data + e * k, A->data + e * j, e);
			j = j + 1;
		}
	}
}

static void insertionsort(char * arr, size_t n, size_t elemsize, compare_fp fp)
{
	size_t c, d;

	void * t = malloc(elemsize);
	for (c = 1; c <= n - 1; c++) {
		d = c;

		while (d > 0 && (fp(arr + d * elemsize, arr + (d - 1) * elemsize) < 0)) {
			memcpy(t, arr + d * elemsize, elemsize);
			memcpy(arr + d * elemsize, arr + (d - 1) * elemsize, elemsize);
			memcpy(arr + (d - 1) * elemsize, t, elemsize);
			d--;
		}
	}
	free(t);
}

// Sort the given run of array A[] using array B[] as a source.
// begin is inclusive; end is exclusive (A->data + end * A->elemsize is not in the set).
static void mergesort(darray* B, size_t begin, size_t end, darray* A,
		compare_fp fp)
{
	/*if (end - begin <= 32) {
	 insertionsort(A->data + A->elemsize*begin, end-begin, A->elemsize, fp);
	 }*/
	if (end - begin < 1) {
		return;                                 //   consider it sorted
	}
	// split the run longer than 1 item into halves
	size_t middle = (end + begin) / 2;              // iMiddle = mid point
	// recursively sort both runs from array A[] into B[]
	mergesort(A, begin, middle, B, fp);  // sort the left  run
	mergesort(A, middle, end, B, fp);  // sort the right run
	// merge the resulting runs from array B[] into A[]
	merge(B, begin, middle, end, A, fp);
}

void da_insertionsort(darray * arr, compare_fp fp)
{
	size_t c, d;
	size_t e = arr->elemsize;
	size_t n = arr->count;

	void * t = malloc(e);
	for (c = 1; c <= n - 1; c++) {
		d = c;
		while (d > 0 && (fp(arr->data + d * e, arr->data + (d - 1) * e) < 0)) {
			memcpy(t, arr->data + d * e, e);
			memcpy(arr->data + d * e, arr->data + (d - 1) * e, e);
			memcpy(arr->data + (d - 1) * e, t, e);
			d--;
		}
	}
	free(t);
}

void da_mergesort(darray* arr, compare_fp fp)
{
	darray * b = da_copy(arr);
	mergesort(b, 0, arr->count, arr, fp); // sort data from B[] into A[]
	da_destroy(b);
}

darray * da_copy(darray * arr)
{
	darray * copy = da_create(arr->elemsize);
	da_reserve(copy, arr->count);
	copy->count = arr->count;
	for (size_t i = 0; i < arr->count; i++) {
		memcpy(copy->data + i * copy->elemsize,
				arr->data + (arr->count - i - 1) * arr->elemsize,
				arr->elemsize);
	}
	return copy;
}
