#ifndef LIST_H_
#define LIST_H_

#include "clib.h"

typedef struct node iterator;
typedef struct list list;

void it_next(iterator ** it);
int it_hasnext(iterator ** it);
void * it_get(iterator ** it);

list* list_copy(list * list);
list* list_reverse(list * list);
list* list_create(size_t elemsize);
list* list_filter1(list* list, filter1_fp fp);
list* list_filter2(list* list, filter2_fp, void *arg);

void list_addback(list * list, void * data);
void list_addfront(list * list, void * data);

void list_foreach1(list * list, foreach1_fp fp);
void list_foreach2(list * list, foreach2_fp fp, void* arg);
void* list_get(list* list, int idx);
void list_insertafter(list* list, iterator * where, void * data);
void list_insertbefore(list* list, iterator * where, void * data);
void list_remove(list* list, iterator * where);
int list_findremove(list * list, compare_fp fp, void * arg);
void list_mergesort(list* list, compare_fp fp);

iterator* list_find(list * list, compare_fp fp, void * arg);
iterator* list_getiterator(list * list);
iterator* list_idx(list * list, int idx);

size_t list_count(list * list);
size_t list_elemsize(list * list);

void list_destroy(list * list);

#endif /* LIST_H_ */
