/*
 ============================================================================
 Name        : clib.c
 Author      :
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <limits.h>
#include "clib.h"
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/poll.h>
#include <pthread.h>
#include <unistd.h>
#include "darray.h"
#include "list2.h"

void list_test(void)
{
	List * l = List_Create();

	List_AddLast(l, Node_FromString("Hello "));
	List_AddLast(l, Node_FromString("World "));
	List_AddLast(l, Node_FromString("Testing "));
	List_AddLast(l, Node_FromString("123 "));

	List *rev = List_Reverse(l);
	char * merge = String_Merge(rev);

	printf("forward: \n");
	for (Node * it = l->first; it != NULL; it = it->next) {
		printf("%s\n", it->data);
	}
	printf("reverse: \n");
	for (Node * it = rev->first; it != NULL; it = it->next) {
		printf("%s\n", it->data);
	}
	printf("merge = %s\n", merge);
	List_Dispose(l);
	List_Dispose(rev);
	free(merge);
}

void tokenizer_test(void)
{
	List * tokens = String_Split("Hello World! Good bye world.", ' ');
	for (Node * it = tokens->first; it != NULL; it = it->next) {
		printf("%s\n", it->data);
	}

	char * joined = String_Join(tokens, '-');
	printf("joined = %s\n", joined);
	free(joined);
	List_Dispose(tokens);
}

void file_test(char * filename)
{
	Array * arr = File_ReadAllBytes(filename);
	int sum = 0;
	if (arr) {
		for (size_t i = 0; i < arr->capacity; i++) {
			sum += arr->data[i];
		}
		File_WriteAllBytes("erfqefqef.bin", arr);
		printf("Read %s of size %lu into memory and then wrote it back.\n", filename, arr->capacity);
		Array_Dispose(arr);
	}
}

void array_test(void)
{
	Array * arr = Array_Create(12);
	Array_Add(arr, (unsigned char*) "hello ", 6);
	Array_Add(arr, (unsigned char*) "world!", 6);
	Array_Add(arr, (unsigned char*) "\r\n", 3); // including zero terminator
	printf("arr->data = %s\n", (char*) arr->data);
	Array_Dispose(arr);
}

void minmaxavg(int * data, int * state)
{
	// update min
	if (data[0] < state[0]) state[0] = data[0];
	// for avgs.
	state[1] += data[0];
	// update max
	if (data[0] > state[2]) state[2] = data[0];
	state[3]++; // count
}
typedef void (*Fold_ptr)(void*data, void*state);
void toarraytest(void)
{
	int tmp = 12345;
	List * l = List_Create();
	Array * a;
	List_AddLast(l, Node_FromBlock((char*) &tmp, sizeof(int)));
	tmp = 1000;
	List_AddLast(l, Node_FromBlock((char*) &tmp, sizeof(int)));
	tmp = 2000;
	List_AddLast(l, Node_FromBlock((char*) &tmp, sizeof(int)));
	tmp = 3000;
	List_AddLast(l, Node_FromBlock((char*) &tmp, sizeof(int)));

	int state[4] = { INT_MAX, 0, INT_MIN, 0 };
	List_Fold(l, (Fold_ptr) minmaxavg, state);
	printf("min=%i, avg=%f, max=%i, n=%i\n", state[0], (double) state[1] / (double) state[3], state[2], state[3]);

	a = List_ToArray(l);
	int * p = (int*) a->data;
	size_t n = a->size / sizeof(int);
	for (size_t i = 0; i < n; i++) {
		printf("%i\n", p[i]);
	}
	List_Dispose(l);
	Array_Dispose(a);
}

void stringreadtest(void)
{
	printf("Please enter something and press enter: \n");
	char * str = String_ReadLine();
	printf("You entered:\n%s\n", str);
	free(str);
}

void testreadalllines(void)
{
	//tests(argv[0]);
	List * lines = File_ReadAllLines("/home/kjell/log.txt", '\n');
	if (lines) {
		File_WriteAllLines("/home/kjell/testlogwrite.txt", lines, '\n');
		List_Dispose(lines);
	}
}

void addsorted(void)
{
	List * l = List_Create();
	char buf[2] = "A";
	for (int i = 0; i < 200; i++) {
		buf[0] = (char) ('A' + (rand() % 26));
		List_AddSort(l, Node_FromString(buf), (cmp_ptr) strcmp);
		//List_Apply(l, printf);
		//printf("\n");
	}
	List_Apply(l, (apply_ptr) printf);
	List_Dispose(l);
	printf("\n");
}

void putint(void * u)
{
	int *i = (int*) u;
	printf("%i ", *i);
}

int intcmp(int * i, int * j)
{
	return i[0] - j[0];
}

void addsortedint(void)
{
	int x;
	List * l = List_Create();
	for (int i = 0; i < 100; i++) {
		x = rand() % 100;
		List_AddSort(l, Node_FromBlock((char*) &x, sizeof(int)), (cmp_ptr) intcmp);

		//printf("\n");
	}
	List_Apply(l, putint);
	List_Dispose(l);
	printf("\n");
}

int IsEven(int*num, int*state)
{
	(void) state;
	if (num[0] & 1) {
		return 0;

	} else {
		return 1;
	}
}

int GreaterThan(int * input, int *value)
{
	if (input[0] > value[0]) {
		return 1;
	} else {
		return 0;
	}
}

int LessThan(int * input, int *value)
{
	if (input[0] < value[0]) {
		return 1;
	} else {
		return 0;
	}
}
/*
 List * listquicksort(List * list, int (*pred)(void*, void*))
 {
 if (!list || !pred)
 return NULL;
 if (!list->first)
 return NULL;

 if(list->first == list->last) {
 List * ret = List_Create();
 List_AddLast(ret, Node_FromNode(list->first));
 return ret;
 }
 List * sorted; // to keep the sorted list in

 List * a = List_Create(); // for values lesser than pivot
 List * b = List_Create(); // for values greater than pivot

 // Exclude pivot point before partition
 List list0 = { list->first->next, list->last };

 // Partition list0 into a and b.
 List_Partition(&list0, a, b, pred, list->first->data);

 // Recursively sort each sub list
 List * asort = listquicksort(a, pred);
 List * bsort = listquicksort(a, pred);

 // For putting the pivot point into the middle
 List_AddLast(asort, list->first);

 // Join both sorted lists
 sorted = List_Join(asort, bsort);

 List_Dispose(a);
 List_Dispose(b);
 //List_Dispose(asort);
 //List_Dispose(bsort);

 return sorted;
 }
 */
void listfiltertest()
{
	List * original = List_Create(); // create a list to hold numbers
	List * evens;
	List * greater;
	int value = 4;

	// fill the original with numbers from 0 to 9
	for (int i = 0; i < 10; i++) {
		List_AddLast(original, Node_FromBlock(&i, sizeof(int)));
	}

	printf("Original list:\n");
	List_Apply(original, putint);
	printf("\n");

	evens = List_Filter(original, (predicate_ptr) IsEven, NULL);
	printf("List with only evens:\n");
	List_Apply(evens, putint);
	printf("\n");

	// Filter original greater than value
	greater = List_Filter(original, (predicate_ptr) GreaterThan, &value);
	printf("List with values greater than %i:\n", value);
	List_Apply(greater, putint);
	printf("\n");

	List_Dispose(evens);
	List_Dispose(greater);
	List_Dispose(original);
}

void tests(char *filename)
{
	if (filename) {
		printf("File test: \n\n");
		file_test(filename);
	}
	printf("List test: \n\n");
	list_test();
	printf("Tokenizer test: \n\n");
	tokenizer_test();
	printf("array test: \n\n");
	array_test();
	printf("toarray test: \n\n");
	toarraytest();
	printf("string read test: \n\n");
	stringreadtest();
	printf("add sorted: \n\n");
	addsorted();
	printf("add sorted int: \n\n");
	addsortedint();
	printf("list filter test: \n\n");
	listfiltertest();
}

/*
 void sorttest(void) {
 List * unsorted = List_Create();
 List * sorted;
 // fill the original with numbers from 0 to 9
 for (int i = 0; i < 10; i++) {
 int r = rand() % 10;
 List_AddLast(unsorted, Node_FromBlock(&r, sizeof(int)));
 }
 sorted = listquicksort(unsorted, LessThan);

 printf("Unsorted list:\n");
 List_Apply(unsorted, putint);
 printf("\n");

 printf("Sorted list:\n");
 List_Apply(sorted, putint);
 printf("\n");

 List_Dispose(unsorted);
 List_Dispose(sorted);
 }
 */

int done = 0;
void userCallback(int fd, void * arg)
{
	(void) arg;
	char data[128];
	size_t n = (size_t) read(fd, data, sizeof(data));
	fwrite(data, 1, n, stdout);
	if (!memcmp(data, "quit", 4)) {
		done = 1;
	}
}

void serialCallback(int fd, void * arg)
{
	(void) arg;
	char data[128];
	size_t n = (size_t) read(fd, data, sizeof(data));
	fwrite(data, 1, n, stdout);
}

void networkCallback(int fd, void * arg)
{
	(void) arg;
	char data[128];
	size_t n = (size_t) read(fd, data, sizeof(data));
	fwrite(data, 1, n, stdout);
}

void serialTest(char * port)
{
	struct termios oldtio;
	int sp = Serialport_Connect(port, B19200, &oldtio);
	struct ReadDataCallback callbacks[2];

	callbacks[0].fd = STDIN_FILENO;
	callbacks[0].callback = userCallback;

	callbacks[1].fd = sp;
	callbacks[1].callback = serialCallback;

	while (!done) {
		Select_Simplified(callbacks, 2);
	}
	tcsetattr(sp, TCSANOW, &oldtio);
}

void ClientTest()
{
	struct termios oldtio;
	int serial_port = Serialport_Connect("/dev/ttyUSB1", B19200, &oldtio);
	int client_socket = TCP_Connect("kkgt.no-ip.org", "45678");

	struct ReadDataCallback callbacks[3];
	callbacks[0].fd = STDIN_FILENO;
	callbacks[0].callback = userCallback;
	callbacks[1].fd = client_socket;
	callbacks[1].callback = networkCallback;
	callbacks[2].fd = serial_port;
	callbacks[2].callback = serialCallback;

	while (!done) {
		if (Select_Simplified(callbacks, 3) == 0) done = 1;
	}

	tcsetattr(serial_port, TCSANOW, &oldtio);
	close(serial_port);
	close(client_socket);
}

int exec_comm_handler(int sck)
{
	close(0); /* close standard input  */
	close(1); /* close standard output */
	close(2); /* close standard error  */

	if (dup(sck) != 0 || dup(sck) != 1 || dup(sck) != 2) {
		perror("error duplicating socket for stdin/stdout/stderr");
		exit(1);
	}

	printf("this should now go across the socket...\n");
	execl("/bin/sh", "/bin/sh", "-c", "/bin/bash", NULL);
	perror("the execl(3) call failed.");
	exit(1);
}

int main_old(int argc, char *argv[])
{
	(void) argc;
	(void) argv;
	struct sockaddr_in peer_addr;
	int client;
	int server_socket = TCP_Server(12345);
	socklen_t addrlen = sizeof(struct sockaddr_in);
	printf("Waiting for connections...\n");
	while (-1 != (client = accept(server_socket, (struct sockaddr*) &peer_addr, &addrlen))) {
		printf("New connection accepted!\n");
		int pid = fork();
		if (pid == 0) {
			dup2(client, 0);
			dup2(client, 1);
			dup2(client, 2);
			printf("this should now go across the socket...\n");
			execl("/bin/bash", "/bin/bash", NULL);
			exit(0);
		} else {
			printf("Spawned process %i!\n", pid);
		}
	}
	return 0;
}

void testudp(char * port)
{

	//struct sockaddr_storage their_addr;
	int udp_sockfd = UDP_Server(port);
	//int tcp_sockfd = TCP_Server(port);
	//char s[INET6_ADDRSTRLEN];
	//socklen_t addr_len = sizeof their_addr;
	struct pollfd ufds[3];
	//Array * clients = Array_Create(sizeof(int));

	ufds[0].fd = STDIN_FILENO;
	ufds[0].events = POLLIN;
	ufds[1].fd = udp_sockfd;
	ufds[1].events = POLLIN;
	//ufds[2].fd = tcp_sockfd;
	//ufds[2].events = POLLIN;

	while (!done) {
		poll(ufds, 2, -1);

		if (ufds[0].revents & POLLIN) {
			Screen_ColorF(2);
			userCallback(ufds[0].fd, NULL);
			Screen_ColorF(7);
		}

		if (ufds[1].revents & POLLIN) {
			Screen_ColorF(1);
			networkCallback(ufds[1].fd, NULL);
			Screen_ColorF(7);
		}
		/*if (ufds[2].revents & POLLIN) {
		 size_t n = read(tcp_sockfd, buf, sizeof(buf));
		 Screen_Color(4);
		 networkCallback(buf, n);
		 Screen_Color(7);
		 }*/

		/*if ((numbytes = recvfrom(sockfd, buf, sizeof(buf) - 1, 0,
		 (struct sockaddr *) &their_addr, &addr_len)) == -1) {
		 perror("recvfrom");
		 exit(1);
		 }

		 printf("listener: got packet from %s\n",
		 inet_ntop(their_addr.ss_family,
		 get_in_addr((struct sockaddr *) &their_addr), s,
		 sizeof s));
		 printf("listener: packet is %d bytes long\n", numbytes);
		 buf[numbytes] = '\0';
		 printf("listener: packet contains \"%s\"\n", buf);*/
	}
	close(udp_sockfd);
	//close(tcp_sockfd);

}

void thread_test()
{
	List * l;

	pthread_t th = Thread_Start((void *(*)(void *)) File_ReadAllLines, "/var/log/dpkg.log");
	sleep(1);
	pthread_join(th, (void**) &l);

	for (Node* it = l->first; it != NULL; it = it->next) {
		char * date = String_Substring((char*) it->data, 0, 10);
		char * time = String_Substring((char*) it->data, 11, 8);
		printf("date='%s' time='%s'\n", date, time);
		free(date);
		free(time);
	}
	List_Dispose(l);

}

void putstr(void * str)
{
	printf("%s\n", (char*) str);
}

void SkrivUt(Iterator * it, void (*print)(void*))
{
	while (It_HasNext(it)) {
		print(It_Get(it));
		It_Next(it);
	}
	It_Dispose(it);
}

void iteratortest(void)
{

	Iterator *it;

	List* l = File_ReadAllLines("/var/log/dpkg.log", '\n');

	EArray * e = EArray_Create(sizeof(int));
	EArray_Resize(e, 10);

	for (it = Range(0, 10); It_HasNext(it); It_Next(it)) {
		int n = *(int*) It_Get(it);
		EArray_Set(e, (size_t) n, &n);
	}
	It_Dispose(it);

	SkrivUt(List_GetIterator(l), putstr);
	SkrivUt(EArray_GetIterator(e), putint);

	List_Dispose(l);
	EArray_Dispose(e);

}

void pipetest(void)
{
	int fds[2];
	char buf[512];
	Pipe_Create("/bin/bash", fds);

	write(fds[1], "pwd\n", 4);
	size_t n = (size_t) read(fds[0], buf, sizeof(buf));
	write(STDOUT_FILENO, buf, n);
	close(fds[0]);
	close(fds[1]);
}

void printint(void * v)
{
	printf("%i ", *(int*) v);
}

int compareint(void* v1, void* v2)
{
	return (*(int*) v1) - (*(int*) v2);
}

int main_old3(int argc, char **argv)
{
	(void) argc;
	(void) argv;

	darray* arr = da_create(sizeof(int));
	for (int i = 0; i < 100; i++) {
		int r = rand() % 100;
		da_addback(arr, &r);
	}

	da_foreach1(arr, printint);
	printf("\n");
	da_mergesort(arr, compareint);
	da_foreach1(arr, printint);
	printf("\n");

	da_destroy(arr);
	/*while (1)
	 free(malloc(128 * 1024 * 1024));*/
	return 0;
}

int isEven(void * v)
{
	return !(*(int*) v & 1);
}

int isOdd(void * v)
{
	return (*(int*) v & 1);
}

int main2(int argc, char **argv)
{
	int mman[4] = { 0 };
	list * l = list_create(sizeof(int));

	for (int i = 0; i < 20; i++) {
		list_addback(l, &i);
	}

	list * evens = list_filter1(l, isEven);
	list * odds = list_filter1(l, isOdd);

	printf("whole list:\n");
	list_foreach1(l, printint);
	memset(mman, 0, sizeof(mman));
	mman[0] = 9999;
	list_foreach2(l, (foreach2_fp) minmaxavg, mman);
	printf("min=%i max=%i avg=%f", mman[0], mman[2], (double) mman[1] / (double) mman[3]);
	printf("\n\nevens list:\n");
	list_foreach1(evens, printint);
	memset(mman, 0, sizeof(mman));
	mman[0] = 9999;
	list_foreach2(evens, (foreach2_fp) minmaxavg, mman);
	printf("min=%i max=%i avg=%f", mman[0], mman[2], (double) mman[1] / (double) mman[3]);
	printf("\n\nodds list:\n");
	list_foreach1(odds, printint);
	memset(mman, 0, sizeof(mman));
	mman[0] = 9999;
	list_foreach2(odds, (foreach2_fp) minmaxavg, mman);
	printf("min=%i max=%i avg=%f", mman[0], mman[2], (double) mman[1] / (double) mman[3]);
	list_destroy(l);
	list_destroy(evens);
	list_destroy(odds);
	return 0;
}

int contains9(void* data, void* state)
{
	return strstr(data, state) != NULL;
}

void colors(void)
{
	for (int i = 0; i < 16; i++) {
		for (int j = 0; j < 16; j++) {
			Screen_Color(i, j);
			printf("%2i,%2i ", i, j);
		}
		Screen_ColorB(0);
		printf("\n");
	}

	Screen_Color(7, 0);
}

#define THS 500

#define N(x) sizeof(x)/sizeof((x)[0])

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;
int go = 0;
void *start_routine(void * arg)
{
	pthread_mutex_lock(&mutex);
	while (go == 0) {
		pthread_cond_wait(&cond, &mutex);
	}
	pthread_mutex_unlock(&mutex);
	int * n = (int*) arg;
	char *res = malloc(256);
	sprintf(res, "Hello from thread %i\n", n[0]);
	n[0]++;
	pthread_exit(res);
	//return res;
}

int main4(/*int argc, char **argv*/void)
{
	int args[THS];
	pthread_t th[THS];
	pthread_cond_init(&cond, NULL);
	for (int i = 0; i < THS; i++) {
		args[i] = i;
		th[i] = Thread_Start(start_routine, &args[i]);
	}

	//pthread_cond_signal(&cond);
	pthread_mutex_lock(&mutex);
	go = 1;
	pthread_cond_broadcast(&cond);
	pthread_mutex_unlock(&mutex);
	for (int i = 0; i < THS; i++) {
		char * res = Thread_Join(th[i]);
		printf("%s", res);
		free(res);
	}

	/*int u$ = 5;
	 printf("%i", u$);*/
	/*List * lines = File_ReadAllLines("/var/log/syslog");
	 if (lines) {
	 List * lines2 = List_Filter(lines, strstr, "seconds");
	 if (lines2) {
	 for (Node * it = lines2->first; it != NULL; it = it->next) {
	 printf("%s\n", it->data);
	 }
	 List_Dispose(lines2);
	 }
	 List_Dispose(lines);
	 }*/
	/*List * lines = List_Create();
	 List_AddLast(lines, Node_FromString("Hello "));
	 List_AddLast(lines, Node_FromString("World!\n"));

	 for (Node * it = lines->first; it != NULL; it = it->next) {
	 printf("%s", it->data);
	 }

	 List_Dispose(lines);*/

	return 0;
}

int main(int argc, char **argv)
{
	rng_t * rng = RNG_Init(3);
	char buf[64];
	RNG_Write(rng, buf, sizeof(buf));
	double u = 0.0, v = 0.0;;
	for(int i = 0; i < 100000; i++) {
		u += RNG_Double(rng)/100.0-u/100.0;
		v += u / 1000.0;
		printf("%i\t%f\t\n",i, u-v);
	}
	free(rng);
return 0;
}

/*int main(int argc, char **argv) {

 EArray * intarr =  EArray_Create(sizeof(int));
 EArray_Resize(intarr, 10);

 for(int i = 0; i < EArray_Size(intarr); i++) {
 int val = i*i;
 EArray_Set(intarr, i, &val);

 }

 EArray_Resize(intarr, 20);

 for(int i = 10; i < EArray_Size(intarr); i++) {
 int val = i*i-100;
 EArray_Set(intarr, i, &val);

 }


 for(int i = 0; i < EArray_Size(intarr); i++) {
 printf("%i. %i\n", i, *(int*)EArray_Get(intarr, i));
 }

 free(intarr);
 return 0;
 }*/

/*int main(int argc, char *argv[])
 {
 int * t;
 char buf[16];
 Array * arr = Array_Create(0);
 for (int i = 0; i < 10; i++) {
 Array_Add(arr, &i, sizeof(int));
 }

 for(int i = 0; i < arr->size/sizeof(int); i++) {
 printf("%i. %i\n", i, t[i]);
 }
 Array_Dispose(arr);

 int fd = Timer_Create();
 Timer_RunInterval(fd, 1, 0);
 for(int i = 0; i < 10; i++) {
 int n = read(fd, buf, 1);
 printf("tick: n=%i\n ... ", n);
 }
 close(fd);
 }*/
